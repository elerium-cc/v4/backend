import { FileClient } from "../../utils/FileConfig";
import { generate } from "randomstring";
import { Request } from "express";
import { extname } from "path";
export default async (req: Request, metaData?: any): Promise<string> => {
    const { file } = req;
    if (file) {
        const fileName = `${generate(8)}${extname(file.originalname)}`
        await FileClient.write(fileName, file.buffer, metaData, file.mimetype);
        return fileName;
    } else {
        throw new Error("File doesn't exist");
    };
};