import express, { json } from 'express';
import config from './utils/dotenv';
if (process.env.NODE_ENV === "development") {
    config();
};

import { 
    UserRouter, 
    InviteRouter,
    FileRouter,
    AdminRouter,
    DomainRouter
} from "./routes";
import cors from "cors";
import { connect } from "mongoose";
import LogClass from "./interfaces/classes/Log";
const Init = new LogClass("init");

const app = express();
app.use(json());
app.use(cors());
app.get('/', (_, res) => res.json({
    success: true,
    message: 'Hello api client'
}));
app.use("/users", UserRouter);
app.use("/invites", InviteRouter);
app.use("/files", FileRouter);
app.use("/admin", AdminRouter);
app.use("/domains", DomainRouter);

connect(process.env.MONGODB_URL!, { 
    useNewUrlParser: true, 
    useUnifiedTopology: true 
}).then(() => { 
    Init.log("Connected to the database")
    app.listen(process.env.PORT, () => Init.log("Web server has been started"))
});