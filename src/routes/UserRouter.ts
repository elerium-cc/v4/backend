import { Router } from 'express';
import { 
    UserModel as User,
    InviteModel
} from "../database/models";
import { 
    LoginSchemaMiddleware, 
    RegistrationSchemaMiddleware 
} from "../interfaces/joi/middlewares";
import AuthMiddleware from "../middlewares/AuthMiddleware";
import NotImplemented from "../utils/res-templetes/NotImplemented";
import MeRouter from "./UserRouterRoutes/MeRouter";

const router = Router();
router.use("/@me", MeRouter);
router.post("/register", RegistrationSchemaMiddleware.body, async (req, res) => {
    const { name, password, invite } = req.body;

    try { 
        await User.register(name, password, invite);
        res.json({ success: true, message: "Your account has been registered you can login to it" });
    } catch (err) {
        const { message } = err;
        res.status(400).json({ success: false, message })
    }
});

router.post("/login", LoginSchemaMiddleware.body, async (req, res) => {
    const { name, password } = req.body;

    try {
        const { token } = await User.login(name, password);
        res.json({ success: true, token });
    } catch (err) { 
        const { message } = err;
        res.status(400).json({ success: false, message })
    };
});

router.get("/:id", NotImplemented);
router.get("/ip/:id", NotImplemented); // prob will not be implemented

export default router;