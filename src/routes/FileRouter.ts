import handleFile from "../pipelines/uploading/handleFile";
import UploadKeyAuthMiddleware from "../middlewares/UploadKeyAuthMiddleware";
import { FileModel } from "../database/models";
import { FileClient } from "../utils/FileConfig";
import { Router } from "express";
import generateToken from "../utils/generateToken";

import Multer, {
    memoryStorage
} from "multer";
import { DeleteFileSchemaMiddleware } from "../interfaces/joi/middlewares";


const upload = Multer({
    storage: memoryStorage()
});
const router = Router();

router.post("/upload", upload.single("file"), UploadKeyAuthMiddleware, async (req, res) => {
    if (!req.user.settings || !req.user.settings.domain) return res.status(401).json({
        success: false,
        message: "You do not have a domain defined in your settings"
    });


    try {
        const deleteKey = generateToken(18);
        const name = await handleFile(req, {
            user: req.user._id,
            deleteKey
        });

        res.json({ 
            success: true,
            url: `${req.user.settings.showUrl ? "\u200b" : ""}` + `${process.env.DOMAIN_USE_HTTPS ? "https" : "http"}://${req.user.settings.domain}/${name}`,
            deleteUrl: `${process.env.API_URL}/files/delete/${name}?k=${deleteKey}`
        });
    } catch (err) {
        res.status(400).json({
            success: false,
            message: err.message
        });
    }
});

router.get("/delete/:file", DeleteFileSchemaMiddleware.query, async (req, res) => {
    const { params, query } = req;

    const file = await FileModel.findOne({
        name: params.file
    });


    if (!file) return res.status(404).json({ 
        success: false,
        message: "File was not found"
    });

    const { metaData } = file;
    if (!metaData) return res.status(400).json({ success: false, message: "There was no data found in the file" });


    if (metaData.deleteKey !== query.k) return res.status(401).json({ 
        success: false,
        message: "Deletion key is invaild"
    });

    try {
        await FileClient.delete(params.file);
        res.json({ 
            success: true,
            message: "File was successfully deleted"
        });
    } catch (err) {
        res.status(
            err.code && 
            typeof err.code == "number" ? 
            err.code : 400
        ).json({
            success: false,
            message: err.message
        }); // copied straight from the image proxy source
    };
})

export default router;