import { Router } from "express";
import AuthMiddleware from "../../middlewares/AuthMiddleware";
import generateToken from "../../utils/generateToken";
import { hash } from "argon2"
import { 
    InviteModel, 
    DomainModel 
} from "../../database/models";
import { 
    DomainSchemaMiddleware,
    UserSettingsSchemaMiddleware,
    LoginSchemaMiddleware,
    PasswordSchemaMiddleware
} from "../../interfaces/joi/middlewares";

const router = Router();
router.use(AuthMiddleware);
router.get("/", (req, res) => {
    const user = req.user.getUser();

    res.json({
        success: true,
        user
    });
});

router.post("/password/set", PasswordSchemaMiddleware.body, async (req, res) => {
    await req.user.setPassword(req.body.password);
    await req.user.updateOne({ password: req.user.password });

    res.json({ success: true, message: "Successfully set your password" });
});

router.post("/invites/create", async (req, res) => {
    if (req.user.invites < 1) return res.status(401).json({
        success: false,
        message: "You do not have enough invites to create"
    });

    const invite = await InviteModel.generateInvite(req.user._id);
    req.user.invites--;

    await req.user.save();

    res.json({ 
        success: true,
        message: invite.code
    });
});

router.post("/settings/domain", DomainSchemaMiddleware.body, async (req, res) => {
    const { body } = req;

    const domain = await DomainModel.findOne({
        name: body.domain
    })

    if (!domain) return res.status(404).json({
        success: false,
        message: "Domain was not found"
    });


    if (body.sub && !domain.wildcard) return res.status(400).json({
        sucesss: false,
        message: "Domain is no wildcarded"
    });


    await req.user.updateOne({ 
        settings: {
            ...req.user.settings,
            domain: body.sub ? `${body.sub}.${body.domain}` : body.domain
        } 
    });

    res.json({ 
        success: true,
        message: "Domain has been successfully changed"
    });
});

router.post("/settings/set", UserSettingsSchemaMiddleware.body, async (req, res) => {
    const { user } = req;

    if (typeof req.body.value == "string" && req.body.type === "showUrl") {
        req.body.value = req.body.value === "true" ? true : false
    };

    if (req.body.type === "showUrl") {
        user.settings.showUrl = req.body.value;
    };

    await user.updateOne({ settings: user.settings });

    res.json({ success: true, message: "Successfully set your setting "}); 
});

router.post("/auth/uploadkey/generate", async (req, res) => {
    const key = generateToken(32);
    req.user.uploadKey = key;

    await req.user.save();

    res.json({ 
        success: true,
        key
    });
})

export default router;