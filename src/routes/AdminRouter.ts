import { Router } from "express";
import AuthMiddleware from "../middlewares/AuthMiddleware";
import AdminMiddleware from "../middlewares/AdminMiddleware";
import { 
    FileModel,
    UserModel,
    InviteModel
} from "../database/models";
import { 
    UserFetchingSchemaMiddleware
} from "../interfaces/joi/middlewares";
import { FileClient } from "../utils/FileConfig";

const router = Router();
router.use(AuthMiddleware);
router.use(AdminMiddleware);

router.get("/", (req, res) => {
    res.json({ success: true });
});

router.get("/files/:filename", async (req, res) => { // most of it will be rewritten
    const {
        params: {
            filename
        }
    } = req;


    const file = await FileModel.findOne({ name: filename });
    if (!file) return res.status(404).json({ success: false, message: "File is not found" });
    if (!file.metaData) return res.status(404).json({ success: false, message: "No metadata was found in the file"});

    const user = await UserModel.findById(file.metaData.user);
    if (!user) return res.status(404).json({ success: false, message: "User was not found"});


    res.json({ success: true, user: user.getUser() });
});

router.get("/user/by-type", UserFetchingSchemaMiddleware.query, async (req, res) => {
    const { query } = req;

    const user = await UserModel.getUserByType(<string>query.type, <string>query.value); // lol

    if (!user) return res.status(404).json({
        success: false,
        message: "User was not found"
    });

    res.json({
        success: true,
        user: user.getUser()
    });
});

router.delete("/files/:filename", async (req, res) => {
    const {
        params: {
            filename
        }
    } = req;

    try {
        await FileClient.delete(filename);
        res.json({ 
            success: true,
            message: "File was successfully deleted"
        });
    } catch (err) {
        res.status(
            err.code && 
            typeof err.code == "number" ? 
            err.code : 400
        ).json({
            success: false,
            message: err.message
        }); // copied straight from the image proxy source
    };    
});

router.post("/invite/generate", async (req, res) => {
    const invite = await InviteModel.generateInvite(req.user._id);

    res.json({
        success: true,
        code: invite.code
    });
});

export default router;