import UserRouter from "./UserRouter";
import InviteRouter from "./InviteRouter";
import FileRouter from "./FileRouter";
import AdminRouter from "./AdminRouter";
import DomainRouter from "./DomainRouter";

export { 
    InviteRouter,
    UserRouter,
    FileRouter,
    AdminRouter,
    DomainRouter
};