import { Router } from "express";
import { DomainModel } from "../database/models";

const router = Router();

router.get("/", async (req, res) => {
    const domains = await DomainModel.find();
    const newArray = [];

    for (const domain of domains) {
        const domainObj = domain.toObject();

        delete domainObj.__v;
        delete domainObj._id;

        newArray.push(domainObj);
    };

    res.json({ success: true, domains: newArray });
});

export default router;