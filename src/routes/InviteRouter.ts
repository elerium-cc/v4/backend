import { Router } from "express";
import { InviteModel as Invite } from "../database/models";
const router = Router();
if (process.env.NODE_ENV == "development") {
    router.post("/generate", async (req, res) => {
        const { code } = await Invite.generateInvite();
        
        res.json({ success: true, code });
    });
};

export default router;