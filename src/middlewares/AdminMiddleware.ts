import { Request, Response, NextFunction } from "express";

export default (req: Request, res: Response, next: NextFunction) => {
    if (req.user && !req.user.admin) return res.status(401).json({
        success: false,
        message: "You are not a admin"
    });

    next();
};