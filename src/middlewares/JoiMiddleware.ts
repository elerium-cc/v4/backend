import { Request, Response, NextFunction } from "express";
import { Schema } from "joi";

export default class JoiMiddleware {
    schema: Schema

    constructor(schema: Schema) {
        this.schema = schema;

        // function bindings
        this.body = this.body.bind(this); 
        this.query = this.query.bind(this);
    };

    private async handle(
        req: Request, 
        data: any, 
        res: Response, 
        next: NextFunction
    ) {
        try {
            const schemaRes = await this.schema.validateAsync(data);
            req.joi = {
                value: schemaRes
            };
            next();
        } catch (err) {
            // { details: { error: {message} }}
            const { details } = err;
            const [ error ] = details;

            res.status(400).json({ success: false, message: error.message });
        };
    };

    async body(req: Request, res: Response, next: NextFunction) {
        const { body } = req;

        await this.handle(req, body, res, next);
    };

    async query(req: Request, res: Response, next: NextFunction) {
        const { query } = req;

        await this.handle(req, query, res, next);
    };
};