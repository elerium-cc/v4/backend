import { Request, Response, NextFunction } from "express";
import { UserModel } from "../database/models";

export default async (req: Request, res: Response, next: NextFunction) => {
    const { 
        headers: {
            authorization
        } 
    } = req;
    
    const user = await UserModel.findOne({ token: authorization });

    if (!user) return res.status(401).json({ 
        success: false,
        message: "Unauthorized"
    });

    if (user.banned) return res.status(401).json({
        success: false,
        message: `Your account has been banned for ${user.banReason ? user.banReason : "reason not specified"}`
    })

    req.user = user;
    next();
};