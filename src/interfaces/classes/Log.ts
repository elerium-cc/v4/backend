export default class LogClass {
    private type: string

    constructor(type: string) {
        this.type = type;
    };

    log(...args: any) {
        console.log(`[${this.type}]`, ...args);
    };
}