// s3 and mongodb file api
import { S3 } from "aws-sdk";
import { FileModel } from "../../database/models";
import FileData from "../FileData";
import FileError from "./FileError";

export default class File { 
    s3: S3
    startKey?: string;
    bucket: string;

    constructor(s3: S3, bucket: string, startKey?: string) {
        this.s3 = s3;
        this.bucket = bucket;
        if (startKey) this.startKey = startKey;

        this.write.bind(this);

    };

    async write(name: string, buf: Buffer, metaData?: any, contentType?: string): Promise<S3.PutObjectOutput> {
        const objKey = this.startKey && `${this.startKey}/${name}` || name;
    

        try {
            const putObjectOut = await this.s3.putObject({
                Bucket: this.bucket,
                Key: objKey,
                Body: buf,
                ContentType: contentType
            }).promise();

            if (!await FileModel.findOne({ name })) await FileModel.create({
                name,
                Key: objKey,
                bucket: this.bucket,
                metaData: metaData
            });

            return putObjectOut;
        } catch(err) {
            throw err
        };
    };

    async delete(name: string): Promise<FileData> {
        const dataObj = await FileModel.findOne({ name });
        if (!dataObj) throw new FileError("File doesn't exist", 404);

        try {
            const S3Response = await this.s3.deleteObject({
                Bucket: this.bucket,
                Key: dataObj.Key
            }).promise();
            
            await dataObj.deleteOne();

            return {
                S3Response,
                metaData: dataObj
            };
        } catch (err) {
            throw err;
        };
    };
};