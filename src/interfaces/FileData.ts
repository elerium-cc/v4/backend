import { S3 } from "aws-sdk";

export default interface FileData {
    metaData: any;
    S3Response: S3.DeleteObjectOutput;
};