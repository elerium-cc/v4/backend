import { string } from "joi";
import LoginSchema from "./LoginSchema"

export default LoginSchema.keys({
    invite: string()
        .required()
        .trim()
});