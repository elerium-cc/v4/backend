import { string, object } from "joi";

export default object({
    password: string() 
        .required()
        .min(8)
        .max(100)
});