import { string, object, any } from "joi";

export default object({
    type: string()
        .required()
        .valid("showUrl"),
    value: any()
        .required()
})