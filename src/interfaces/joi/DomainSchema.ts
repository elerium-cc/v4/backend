import { object, string } from "joi";

export default object({
    domain: string()
        .required()
        .domain({
            minDomainSegments: 2
        }),
    sub: string()
});