import JoiMiddleware from "../../middlewares/JoiMiddleware";

import { 
    UserSchema,
    LoginSchema,
    RegistrationSchema,
    DomainSchema,
    DeleteFileSchema,
    UserFetchingSchema,
    UserSettingsSchema,
    PasswordSchema
} from "./";

export const UserSchemaMiddleware = new JoiMiddleware(UserSchema);
export const LoginSchemaMiddleware = new JoiMiddleware(LoginSchema);
export const RegistrationSchemaMiddleware = new JoiMiddleware(RegistrationSchema);
export const DomainSchemaMiddleware = new JoiMiddleware(DomainSchema);
export const DeleteFileSchemaMiddleware = new JoiMiddleware(DeleteFileSchema);
export const UserFetchingSchemaMiddleware = new JoiMiddleware(UserFetchingSchema);
export const UserSettingsSchemaMiddleware = new JoiMiddleware(UserSettingsSchema);
export const PasswordSchemaMiddleware = new JoiMiddleware(PasswordSchema);