import { object, string } from "joi";

export default object({
    name: string()
        .required()
        .token()
        .min(5)
        .max(20)
});