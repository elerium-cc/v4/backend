import { string } from "joi";
import UserSchema from "./UserSchema";

export default UserSchema.keys({
    password: string() 
        .required()
        .min(8)
        .max(100)
});