import { string, object } from "joi";


export default object({
    type: string()
        .required()
        .valid(
            "name",
            "key"
        ),
    value: string()
        .required()
});