import { 
    prop,
    getModelForClass,
    modelOptions
} from "@typegoose/typegoose";

@modelOptions({
    options: {
        allowMixed: 0
    }
})
export class Domain {
    @prop({ required: true })
    public name!: string;

    @prop({ default: false })
    public wildcard?: boolean;
}


export default getModelForClass(Domain);