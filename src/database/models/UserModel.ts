import { 
    prop, 
    getModelForClass,
    DocumentType,
    ReturnModelType,
    modelOptions
} from "@typegoose/typegoose"


import RegistrationError from "../../interfaces/classes/RegistrationError";
import LoginError from "../../interfaces/classes/LoginError";
import generateString from "../../utils/generateToken";
import inviteModel from "./InviteModel";

import { 
    hash, 
    verify 
} from "argon2";

@modelOptions({
    options: {
        allowMixed: 0
    }
})
export class User { 
    @prop({ required: true })
    public name!: string;

    @prop({ required: true })
    public password!: string; 

    @prop({ required: true })
    public token!: string;

    @prop({ default: true })
    public canRefToken?: boolean; // used for the super user account

    @prop({ default: false })
    public admin!: boolean;

    @prop({ default: 0 })
    public invites?: number;

    @prop({ default: false })
    public banned?: boolean;

    @prop()
    public banReason?: string;


    @prop({ })
    public uploadKey?: string;

    @prop()
    public settings?: {
        domain?: string;
        showUrl?: boolean;
    };

    public static async register(
        this: ReturnModelType<typeof User>, 
        name: string, 
        password: string,
        invite?: string
    ): Promise<DocumentType<User>> {
        const userModel = this;
        const user = new userModel();

        if (invite) {
            let inviteDoc = await inviteModel.findOne({ code: invite });
            if (!inviteDoc || inviteDoc.redeemed) throw new RegistrationError("Invite code was redeemed or invaild");

            inviteDoc.redeemed = true;
            await inviteDoc.save();
        };
        
        if (
            await userModel.findOne({ name })
        ) throw new RegistrationError("The username was claimed already");
        

        user.name = name;
        user.token = generateString(24);
        user.password = await hash(password);
        user.settings = {};
        if (process.env.DEFAULT_DOMAIN) user.settings.domain = process.env.DEFAULT_DOMAIN;
        
        await user.save();

        return user;
    };


    public static async login(
        this: ReturnModelType<typeof User>,
        name: string, 
        password: string
    ): Promise<DocumentType<User>> {
        const userModel = this;

        const user = await userModel.findOne({ name });
        if (!user) throw new LoginError("Username is invaild");
    
        if (!await verify(user.password, password)) throw new LoginError("Password is invaild");

        return user;
    };


    public getUser(this: DocumentType<User>) {
        const userDoc = this;

        const user = userDoc.toObject();

        delete user.__v;
        delete user.password;

        return user;
    };

    public async setPassword(this: DocumentType<User>, password: string) {
        const userDoc = this;
        userDoc.password = await hash(password);
    };

    public static async getUserByType(
        this: ReturnModelType<typeof User>,
        type: string, 
        value: string
    ) {
        const userModel = this;
        let user;

        if (type == "key") user = await userModel.findOne({ uploadKey: value });
        if (type == "name") user = await userModel.findOne({ name: value });

        return user;
    };

};

export default getModelForClass(User);