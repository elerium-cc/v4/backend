import { 
    prop, 
    getModelForClass,
    DocumentType,
    ReturnModelType,
} from "@typegoose/typegoose"
import { Types } from "mongoose"
import generateString from "../../utils/generateToken";

export class Invite { 
    @prop({ required: true})
    public code!: string;

    @prop({ default: false })
    public redeemed?: boolean;

    @prop({ })
    public createdBy?: Types.ObjectId

    public static async generateInvite(this: ReturnModelType<typeof Invite>, creator?: Types.ObjectId): Promise<DocumentType<Invite>> {
        const inviteModel = this;

        const invite = new inviteModel();
        invite.code = generateString(35);
        if (creator) invite.createdBy = creator;

        await invite.save();

        return invite;
    };
}

export default getModelForClass(Invite);