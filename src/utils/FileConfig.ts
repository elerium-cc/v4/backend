import { S3 } from "aws-sdk";
import File from "../interfaces/classes/File";

const s3Client = new S3({
    endpoint: process.env.MINIO_URL,
    accessKeyId: process.env.MINIO_ACCESS_KEY,
    secretAccessKey: process.env.MINIO_SECRET_KEY,
    s3ForcePathStyle: true
});
const bucket = process.env.MINIO_BUCKET;

export const FileClient = new File(s3Client, bucket!, "uploads");