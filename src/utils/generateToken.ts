import { randomBytes } from "crypto";

export default (len: number) => randomBytes(len)
    .toString("hex");