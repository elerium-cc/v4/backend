import User from "../src/database/user";
import { DocumentType } from "@typegoose/typegoose"

declare global {
    namespace Express {
        interface Request { 
            user: DocumentType<User>;
            joi: {
                value: any;
            };
        };
    };
};